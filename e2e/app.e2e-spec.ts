import { UjianFEPage } from './app.po';

describe('ujian-fe App', function() {
  let page: UjianFEPage;

  beforeEach(() => {
    page = new UjianFEPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
