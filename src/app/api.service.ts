import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class APIService{

constructor(private http:Http) { 
    this.http.get('https://jsonplaceholder.typicode.com/users')
    .map(res=>res.json())
    .catch(error => Observable.throw(error.json().error) || "Server Error")
    .subscribe(result => this.userlist = result);
  }

userlist:Object[];


}


