import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'useradd',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {

  constructor(private api:APIService) { }

  name:string = "";
  email:string = "";
  address:string= "";
  phone:number= null;
  company:string="";
  id:number=null;
  ngOnInit() {
    this.id=this.api.userlist.length+1
  }

   addBook(){
    this.api.userlist.push({"id":this.id,"name":this.name, "email":this.email, 
    "address":{"street":this.address}, "company":{"name":this.company}});
    this.name = "";
    this.email = "";
    this.address = "";
    this.phone = null;
    this.company ="";
  }

}
