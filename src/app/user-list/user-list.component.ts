import { Component, OnInit } from '@angular/core';
import { APIService } from '../api.service';

@Component({
  selector: 'userlist',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(private api:APIService) { }

  remove(index){
    this.api.userlist.splice(index,1);
  }

  ngOnInit() {
    
  }
  
 
}
